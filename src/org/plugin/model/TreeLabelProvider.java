package org.plugin.model;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

public class TreeLabelProvider implements ILabelProvider {

	@Override
	public void addListener(ILabelProviderListener arg0) {

		
	}

	@Override
	public void dispose() {

		
	}

	@Override
	public boolean isLabelProperty(Object arg0, String arg1) {

		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener arg0) {

		
	}

	@Override
	public Image getImage(Object arg0) {
		ImageDescriptor imgDescriptor = PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJ_ELEMENT);
		
		return imgDescriptor.createImage();
	}

	@Override
	public String getText(Object arg0) {
		return arg0.toString();
	}

}
